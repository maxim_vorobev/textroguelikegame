package classes.gameCore;

import classes.BaseDungeon;
import classes.CharacterClass;
import classes.Enemy;
import classes.dungeons.Forest;
import classes.enemies.Snake;

public abstract class GameCore {
    public static boolean checkEnemyAlive(Enemy enemy, CharacterClass hero){
       if (enemy.getHealthPoints() == 0){
           clearConsole(2);
            System.out.println(enemy.getName() + " побеждён! ✅");
           if (enemy instanceof Snake){
               hero.setAttackAmount(hero.getHeroWeapon().getWeaponAttackValue());
           }

           enemy.dropLoot(hero);
           enemy.dropXP(hero);
            return false;
        }
        return true;
    }

    public static boolean checkHeroAlive(CharacterClass hero){
        if (hero.getHealthPoints() == 0) {
            clearConsole(2);
            return false;
        }
        return true;
    }

    public static boolean startGameLoop(CharacterClass hero, BaseDungeon dungeon){
            System.out.println(hero.getName() + " ЗАХОДИТ В ЛЕС \uD83C\uDF32 \uD83C\uDF32 \uD83C\uDF32");

            for (int i = 0; i < dungeon.getRoomCount(); i++) {
                GameCore.clearConsole(1);
                System.out.println("⬆\uFE0FУровень " +(i + 1)+ " локации " + dungeon.getName() + "⬆\uFE0F");
                clearConsole(1);

                Enemy enemy = Forest.generateBattle();
                System.out.println("\n");
                System.out.println("⚔\uFE0F⚔\uFE0F⚔\uFE0FНачинается бой против " + enemy.getName() + " ("+ enemy.getHealthPoints()+"\uD83D\uDC97 "+enemy.getAttackAmount()+"\uD83D\uDCA5)⚔\uFE0F⚔\uFE0F⚔\uFE0F");
                System.out.println("\n");

                while (fightAction(hero, enemy)){
                    clearConsole(1);
                    hero.healHealth();
                    hero.restoreMana();
                }
                if (!GameCore.checkHeroAlive(hero)){
                    System.out.println(hero.getName() + " помер...☠\uFE0F☠\uFE0F☠\uFE0F");
                    return false;
                }

            }
            return true;
    }

    public static void clearConsole(int count){
        System.out.println("\n".repeat(count));
    }

    public static boolean fightAction(CharacterClass hero, Enemy enemy){
        while (GameCore.checkEnemyAlive(enemy, hero) && GameCore.checkHeroAlive(hero)){

            enemy.attack(hero);
            if (!GameCore.checkHeroAlive(hero)){
                return false;
            }

            hero.chooseFightAction(enemy);
            if (!GameCore.checkEnemyAlive(enemy, hero)){
                return false;
            }

            return true;

        }
        return false;
    }

    public static double getRandomIntegerBetweenRange(double min, double max){
        double x = (int)(Math.random()*((max-min)+1))+min;
        return x;
    }

    public static void notЕnoughManaAlert(){
        GameCore.clearConsole(1);
        System.out.println("⚠\uFE0FНедостаточно маны для навыка оружия!⚠\uFE0F");
        GameCore.clearConsole(1);
    }
}
