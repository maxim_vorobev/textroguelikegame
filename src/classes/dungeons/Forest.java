package classes.dungeons;

import classes.BaseDungeon;
import classes.Enemy;
import classes.enemies.ForestBoar;
import classes.enemies.Snake;
import classes.enemies.StoneGolem;
import classes.enemies.Wolf;
import classes.gameCore.GameCore;


public class Forest extends BaseDungeon {


    public Forest() {
        this.setName("Лес");
        this.setRoomCount(10);
    }

    public static Enemy generateBattle(){
        int witchEnemy = (int) GameCore.getRandomIntegerBetweenRange(1, 4);

        if (witchEnemy == 1){
            return new Snake();
        } else if (witchEnemy == 2){
            return new Wolf();
        }else if (witchEnemy == 3){
            return new ForestBoar();
        }else if (witchEnemy == 4){
            return new StoneGolem();
        }else{
            System.out.println("РАНДОМ ПРИ ГЕНЕРАЦИИ НЕ СРАБОТАЛ, ДАЮ ЗМЕЮ");
            return new Snake();
        }

    }

}
