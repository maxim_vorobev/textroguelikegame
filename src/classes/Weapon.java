package classes;

public abstract class Weapon implements BaseWeapon{
    private int weaponAttackValue;
    private String weaponName;
    private String weaponSkillName;
    private int spellManacost;

    public Weapon() {
    }

    public int getWeaponAttackValue() {
        return weaponAttackValue;
    }

    public void setWeaponAttackValue(int weaponAttackValue) {
        this.weaponAttackValue = weaponAttackValue;
    }

    public String getWeaponName() {
        return weaponName;
    }

    public void setWeaponName(String weaponName) {
        this.weaponName = weaponName;
    }

    public String getWeaponSkillName() {
        return weaponSkillName;
    }

    public void setWeaponSkillName(String weaponSkillName) {
        this.weaponSkillName = weaponSkillName;
    }

    public int getSpellManacost() {
        return spellManacost;
    }

    public void setSpellManacost(int spellManacost) {
        this.spellManacost = spellManacost;
    }

    public abstract boolean weaponSkill(CharacterClass hero, Enemy enemy);
}
