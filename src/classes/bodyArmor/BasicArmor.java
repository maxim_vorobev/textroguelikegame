package classes.bodyArmor;

import classes.BodyArmor;

public class BasicArmor extends BodyArmor {
    public BasicArmor() {
        this.setName("Лёгкий поношенный нагрудник");
        this.setArmorAmount(0);
    }
}
