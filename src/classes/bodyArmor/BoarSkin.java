package classes.bodyArmor;

import classes.BodyArmor;

public class BoarSkin extends BodyArmor {
    public BoarSkin() {
        this.setName("Броня из шкуры кабана");
        this.setArmorAmount(1);
        this.setHealthRegenAmount(2);
    }
}
