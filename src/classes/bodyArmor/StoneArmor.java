package classes.bodyArmor;

import classes.BodyArmor;

public class StoneArmor extends BodyArmor {
    public StoneArmor() {
        this.setName("Каменная броня");
        this.setArmorAmount(4);
        this.setManaRegenAmount(1);
    }
}
