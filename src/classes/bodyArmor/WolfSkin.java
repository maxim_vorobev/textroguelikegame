package classes.bodyArmor;

import classes.BodyArmor;

public class WolfSkin extends BodyArmor {
    public WolfSkin() {
        this.setName("Броня из волчьей шкуры");
        this.setArmorAmount(2);
        this.setManaRegenAmount(1);
    }
}
