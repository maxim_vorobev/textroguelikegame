package classes;

import classes.characters.Hero;

import java.io.IOException;

public abstract class BaseDungeon {
    private int roomCount;
    private String name;
    public BaseDungeon() {

    }

    public int getRoomCount() {
        return roomCount;
    }

    public void setRoomCount(int roomCount) {
        this.roomCount = roomCount;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
