package classes;

public interface BaseClass {
    void attack(Enemy enemy);
    void restoreHealth(int amount);
    void loseHealth(int amount);
    void restoreMana(int amount);
    void loseMana(int amount);
    void levelUp();
    void info();
    void chooseEquipArmor(BodyArmor armor, Enemy enemy);
    void chooseEquipWeapon(Weapon weapon, Enemy enemy);



}
