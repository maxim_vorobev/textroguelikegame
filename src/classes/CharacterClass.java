package classes;

import classes.gameCore.GameCore;

import java.util.Scanner;
import classes.activeSkills.*;

public abstract class CharacterClass implements BaseClass{
   private int healthPoints;
   private int manaPoints;
   private int level;
   private Weapon heroWeapon;
   private int attackAmount;
   private String name;
   private int maxHealthPoints;
   private int maxManaPoints;
   private BodyArmor bodyArmor;
   private int armorAmount;
   private int healthRegenAmount;
   private int manaRegenAmount;
   private int currentXP;
   private boolean increasedReaction;
   private boolean berserkMode;
   private boolean anExperiencedSorcerer;
   private boolean devourTheSoul;
   private boolean bloodRituals;
   private boolean totalRegeneration;


    public void setCurrentXP(int currentXP) {
        this.currentXP = currentXP;
        if (this.currentXP >= 100){
            this.currentXP = this.currentXP - 100;
            this.levelUp();
        }
    }

    @Override
    public void levelUp() {
        for (int i = 0; i < 1000; i++) {
            int action = (int) GameCore.getRandomIntegerBetweenRange(1,6);

            if (action == 1 && !anExperiencedSorcerer){
                this.anExperiencedSorcerer = true;
                System.out.println("\n");
                System.out.println("⬆\uFE0F⬆\uFE0F⬆\uFE0FLVL UP⬆\uFE0F⬆\uFE0F⬆\uFE0F");
                System.out.println("Получен навык 'Опытный колдун'(пассивный). Теперь при использовании способности оружия есть шанс восстановить ману");
                break;
            } else  if (action == 2 && !berserkMode){
                this.berserkMode = true;
                System.out.println("\n");
                System.out.println("⬆\uFE0F⬆\uFE0F⬆\uFE0FLVL UP⬆\uFE0F⬆\uFE0F⬆\uFE0F");
                System.out.println("Получен навык 'Режим берсерка'(пассивный). Теперь есть шанс атаковать дважды");
                break;
            }else  if (action == 3 && !increasedReaction){
                this.increasedReaction = true;
                System.out.println("\n");
                System.out.println("⬆\uFE0F⬆\uFE0F⬆\uFE0FLVL UP⬆\uFE0F⬆\uFE0F⬆\uFE0F");
                System.out.println("Получен навык 'Повышенная реакция'(пассивный). Теперь есть шанс увернуться от атаки противника");
                break;
            }else  if (action == 4 && !devourTheSoul){
                this.devourTheSoul = true;
                System.out.println("\n");
                System.out.println("⬆\uFE0F⬆\uFE0F⬆\uFE0FLVL UP⬆\uFE0F⬆\uFE0F⬆\uFE0F");
                System.out.println("Получен навык 'Поглощение души'(активный). За 40\uD83D\uDD2E вы мгновенно убиваете врага, восстанавливая себе \uD83D\uDC97 в размере \uD83D\uDC97 врага");
                break;
            }else  if (action == 5 && !bloodRituals){
                this.bloodRituals = true;
                System.out.println("\n");
                System.out.println("⬆\uFE0F⬆\uFE0F⬆\uFE0FLVL UP⬆\uFE0F⬆\uFE0F⬆\uFE0F");
                System.out.println("Получен навык 'Кровавый ритуал'(активный). За 15❤\uFE0F вы привываете 3 бесов, которые наносят случайный урон противнику");
                break;
            }else  if (action == 6 && !totalRegeneration){
                this.totalRegeneration = true;
                System.out.println("\n");
                System.out.println("⬆\uFE0F⬆\uFE0F⬆\uFE0FLVL UP⬆\uFE0F⬆\uFE0F⬆\uFE0F");
                System.out.println("Получен навык 'Тотальная регенерация'(пассивный). ❤\uFE0F\u200D\uD83E\uDE79 повышено на 1. \uD83D\uDD2E\uD83E\uDE79 повышено на 2.");
                this.healthRegenAmount += 1;
                this.manaRegenAmount += 2;
                break;
            }

        }
    }

    public void healHealth(){
        if (!(this.healthPoints == this.maxHealthPoints) && !(this.getHealthRegenAmount() == 0)){
            this.healthPoints += this.healthRegenAmount + this.bodyArmor.getHealthRegenAmount();
            System.out.println(this.name + " восстанавливает " + (int)(this.healthRegenAmount + this.bodyArmor.getHealthRegenAmount()) + "\uD83D\uDC97");
            GameCore.clearConsole(1);
        }
    }

    public void restoreMana(){
        if (!(this.manaRegenAmount == this.maxManaPoints) && !(this.getManaRegenAmount() == 0)){
            this.manaPoints += this.manaRegenAmount + this.bodyArmor.getManaRegenAmount();
            System.out.println(this.name + " восстанавливает " + (int)(this.manaRegenAmount + this.bodyArmor.getManaRegenAmount()) + "\uD83D\uDD2E");
            GameCore.clearConsole(1);
        }
    }

   public void chooseFightAction(Enemy enemy){
       while (true){
           System.out.println("\n❓❓❓❓❓❓❓❓❓❓❓❓❓❓❓❓❓❓❓❓❓❓❓❓❓❓❓❓❓❓❓❓❓❓❓❓❓❓❓\n");
           System.out.println("Что сделает герой? (" + this.getHealthPoints()+"\uD83D\uDC97 "+this.getManaPoints()+"\uD83D\uDD2E "+ this.getArmorAmount() +"\uD83D\uDEE1\uFE0F " + this.getHealthRegenAmount() +"❤\uFE0F\u200D\uD83E\uDE79 " + this.getManaRegenAmount()+"\uD83D\uDD2E\uD83E\uDE79)  \n 1 - Обычная атака("+this.getAttackAmount()+"⚔\uFE0F) \n 2 - Навык оружия("+this.getHeroWeapon().getWeaponSkillName()+" : нужно "+ this.getHeroWeapon().getSpellManacost() + " маны\uD83D\uDD2E)");

           //для перка поглощение души
           if(devourTheSoul){
               System.out.println(" душа - Поглотить душу\uD83D\uDC7B (60\uD83D\uDD2E)");
           }

           //для перка кровавые ритуалы
           if(bloodRituals){
               System.out.println(" кровь - Совершить кровавый ритуал\uD83D\uDC79\uD83E\uDE78 (- 15\uD83D\uDC97)");
           }

           Scanner console = new Scanner(System.in);
           String action = console.nextLine();
           System.out.println("\n\n");
           if(action.equals("1")){
               this.attack(enemy);
               break;
           } else if(action.equals("2")){
               if (this.getHeroWeapon().weaponSkill(this, enemy)){
                   //для перка опытный колдун
                   int innerAction = (int) GameCore.getRandomIntegerBetweenRange(1,4);
                   if (innerAction == 1 && this.anExperiencedSorcerer){
                       System.out.println(this.name + " слишком опытный колдун, чтобы тратить ману попусту\uD83E\uDDD9 Восстановлено 10\uD83D\uDD2E");
                       this.setManaPoints(this.getManaPoints() + 10);
                   }

                   break;
               }
           } else if(action.equals("душа")){
               if (ActiveSkillsLIB.devourSoul(this, enemy)){
                   break;
               }
           } else if(action.equals("кровь")){
               if (ActiveSkillsLIB.bloodRituals(this, enemy)){
                   break;
               }
       } else {
               System.out.println("⚠\uFE0F⚠\uFE0F⚠\uFE0FОшибка! Попробуйте снова⚠\uFE0F⚠\uFE0F⚠\uFE0F");
           }
       }
   }

    public void chooseEquipArmor(BodyArmor armor, Enemy enemy){
        while (true){
            if(armor.getName().equals(bodyArmor.getName())){
                break;
            }
            System.out.println("\n");
            System.out.println("Из " + enemy.getName() + " выпала броня!");
            System.out.println("\n❓❓❓❓❓❓❓❓❓❓❓❓❓❓❓❓❓❓❓❓❓❓❓❓❓❓❓❓❓❓❓❓❓❓❓❓❓❓❓\n");
            System.out.println("Надеть броню?("+ armor.getName() +") \n 1 - Надеть("+armor.getArmorAmount()+"\uD83D\uDEE1\uFE0F "+ armor.getHealthRegenAmount() +"❤\uFE0F\u200D\uD83E\uDE79 "+ armor.getManaRegenAmount() +"\uD83D\uDD2E\uD83E\uDE79) \n 2 - Выбросить");

            Scanner console = new Scanner(System.in);
            String action = console.nextLine();
            if(action.equals("1")){
                this.setBodyArmor(armor);
                break;
            } else if(action.equals("2")){
                break;
            } else {
                System.out.println("⚠\uFE0F⚠\uFE0F⚠\uFE0FНедопустимые символы! Попробуйте снова⚠\uFE0F⚠\uFE0F⚠\uFE0F");
            }
        }
    }

    public void chooseEquipWeapon(Weapon weapon, Enemy enemy){
        while (true){
            if(weapon.getWeaponName().equals(heroWeapon.getWeaponName())){
                break;
            }
            System.out.println("\n");
            System.out.println("Из " + enemy.getName() + " выпало оружие!");
            System.out.println("\n❓❓❓❓❓❓❓❓❓❓❓❓❓❓❓❓❓❓❓❓❓❓❓❓❓❓❓❓❓❓❓❓❓❓❓❓❓❓❓\n");
            System.out.println("Взять оружие?("+ weapon.getWeaponName() +") \n 1 - Надеть("+weapon.getWeaponAttackValue()+"⚔\uFE0F) \n 2 - Выбросить");
            Scanner console = new Scanner(System.in);
            String action = console.nextLine();
            if(action.equals("1")){
                this.setHeroWeapon(weapon);
                break;
            } else if(action.equals("2")){
                break;
            } else {
                System.out.println("⚠\uFE0F⚠\uFE0F⚠\uFE0FНедопустимые символы! Попробуйте снова⚠\uFE0F⚠\uFE0F⚠\uFE0F");
            }
        }
    }

    @Override
    public void attack(Enemy enemy) {
       System.out.println("\uD83D\uDC94"+this.name + " атакует и наносит " + this.attackAmount + " урона!\uD83D\uDC94");
       enemy.loseHealth(this.attackAmount);

        //для перка берсерк
        int action = (int) GameCore.getRandomIntegerBetweenRange(1,6);
        if (action == 1 && this.berserkMode){
            System.out.println(this.name + " впадает в режим берсерка\uD83E\uDD2C\uD83D\uDCA2\uD83D\uDCA5");
            System.out.println("\uD83D\uDC94"+this.name + " атакует и наносит " + attackAmount + " урона!\uD83D\uDC94");
            enemy.loseHealth(attackAmount);
        }

       System.out.println("\uD83D\uDC97У " + enemy.getName() + " осталось "+ enemy.getHealthPoints() + " жизней. \uD83D\uDC97");
    }

    public void attack(Enemy enemy, int attackAmount) {
        System.out.println("\uD83D\uDC94"+this.name + " атакует и наносит " + attackAmount + " урона!\uD83D\uDC94");
        enemy.loseHealth(attackAmount);

        //для перка берсерк
        int action = (int) GameCore.getRandomIntegerBetweenRange(1,6);
        if (action == 1 && this.berserkMode){
            System.out.println(this.name + " впадает в режим берсерка\uD83E\uDD2C\uD83D\uDCA2\uD83D\uDCA5");
            System.out.println("\uD83D\uDC94"+this.name + " атакует и наносит " + attackAmount + " урона!\uD83D\uDC94");
            enemy.loseHealth(attackAmount);
        }

        System.out.println("\uD83D\uDC97У " + enemy.getName() + " осталось "+ enemy.getHealthPoints() + " жизней. \uD83D\uDC97");
    }

    @Override
    public void restoreHealth(int amount) {
        setHealthPoints(this.healthPoints  + amount);
    }

    @Override
    public void loseHealth(int amount) {

        //для перка повышенная реакция
        int action = (int) GameCore.getRandomIntegerBetweenRange(1,4);
        if (action == 1 && this.increasedReaction){
            System.out.println(this.name + " уворачивается от урона с помощью повышенной реакции\uD83D\uDCA8");
            return;
        }

       if (amount > this.armorAmount + this.bodyArmor.getArmorAmount()){
           this.healthPoints -= amount - (this.armorAmount + this.bodyArmor.getArmorAmount());
       } else {
           this.healthPoints -= 1;
       }

        if (this.healthPoints < 0) {
            this.healthPoints= 0;
        }
    }

    @Override
    public void restoreMana(int amount) {
        setManaPoints(this.manaPoints + amount);
    }

    @Override
    public void loseMana(int amount) {
        setManaPoints(this.manaPoints - amount);
    }

    @Override
    public void info() {
        System.out.println("Name:" + this.name + "\nЗдоровье: " + this.healthPoints + "\nМана: " + this.manaPoints + "\nУровень: " + this.level + "\nОружие: " + this.heroWeapon.getWeaponName());
    }

    public int getHealthPoints() {
        return healthPoints;
    }

    public void setHealthPoints(int healthPoints) {
        if (healthPoints < 0) this.healthPoints = 0;
        else if (healthPoints > this.maxHealthPoints) this.healthPoints = this.maxHealthPoints;
        else this.healthPoints = healthPoints;
    }

    public int getManaPoints() {
        return manaPoints;
    }

    public void setManaPoints(int manaPoints) {
        if (manaPoints < 0) this.manaPoints = 0;
        else if (manaPoints > this.maxManaPoints) this.manaPoints = this.maxManaPoints;
        else this.manaPoints = manaPoints;


    }

    public void setLevel(int level) {
        if (level < 0) System.out.println("Уровень не может быть меньше нуля!");
        else this.level = level;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getMaxManaPoints() {
        return maxManaPoints;
    }

    public void setMaxManaPoints(int maxManaPoints) {
        this.maxManaPoints = maxManaPoints;
    }

    public int getMaxHealthPoints() {
        return maxHealthPoints;
    }

    public void setMaxHealthPoints(int maxHealthPoints) {
        this.maxHealthPoints = maxHealthPoints;
    }

    public Weapon getHeroWeapon() {
        return heroWeapon;
    }

    public void setHeroWeapon(Weapon heroWeapon) {
        this.heroWeapon = heroWeapon;
        this.setAttackAmount();
    }

    public int getAttackAmount() {
        return attackAmount;
    }

    public void setAttackAmount() {
        this.attackAmount = this.heroWeapon.getWeaponAttackValue();
    }



    public void setAttackAmount(int attackAmount) {
        this.attackAmount = attackAmount;
        if (this.attackAmount < 0){
            this.attackAmount = 1;
        }
    }

    public BodyArmor getBodyArmor() {
        return bodyArmor;
    }

    public void setBodyArmor(BodyArmor bodyArmor) {
        this.bodyArmor = bodyArmor;
    }

    public int getArmorAmount() {
        return armorAmount + this.bodyArmor.getArmorAmount();
    }

    public void setArmorAmount(int armorAmount) {
        this.armorAmount = armorAmount;
    }

    public int getHealthRegenAmount() {
        return healthRegenAmount + this.bodyArmor.getHealthRegenAmount();
    }

    public void setHealthRegenAmount(int healthRegenAmount) {
        this.healthRegenAmount = healthRegenAmount;
    }

    public int getManaRegenAmount() {
        return this.manaRegenAmount + this.bodyArmor.getManaRegenAmount();
    }

    public void setManaRegenAmount(int manaRegenAmount) {
        this.manaRegenAmount = manaRegenAmount;
    }
    public int getCurrentXP() {
        return currentXP;
    }

    public void setIncreasedReaction(boolean increasedReaction) {
        this.increasedReaction = increasedReaction;
    }

    public void setBerserkMode(boolean berserkMode) {
        this.berserkMode = berserkMode;
    }

    public void setAnExperiencedSorcerer(boolean anExperiencedSorcerer) {
        this.anExperiencedSorcerer = anExperiencedSorcerer;
    }

    public void setDevourTheSoul(boolean devourTheSoul) {
        this.devourTheSoul = devourTheSoul;
    }

    public void setBloodRituals(boolean bloodRituals) {
        this.bloodRituals = bloodRituals;
    }

    public void setTotalRegeneration(boolean totalRegeneration) {
        this.totalRegeneration = totalRegeneration;
    }
}
