package classes.weapons;

import classes.CharacterClass;
import classes.Enemy;
import classes.Weapon;
import classes.gameCore.GameCore;

public class BasicSword extends Weapon {
    public BasicSword() {
        super();
        this.setWeaponName("Ржавый меч");
        this.setWeaponAttackValue(5);
        this.setWeaponSkillName("Отчаянный удар");
        this.setSpellManacost(5);
    }

    @Override
    public boolean weaponSkill(CharacterClass hero, Enemy enemy) {
        if (hero.getManaPoints() - this.getSpellManacost() >= 0 && hero.getHealthPoints() - 5 > 0) {
            hero.setManaPoints(hero.getManaPoints() - this.getSpellManacost());
            hero.setHealthPoints(hero.getHealthPoints() - 5);
            System.out.println(hero.getName() + " совершает " + this.getWeaponSkillName() + " и наносит себе 5 урона");
            hero.attack(enemy, 15);
            return true;
        } else {
            GameCore.notЕnoughManaAlert();
            return false;
        }
    }
}
