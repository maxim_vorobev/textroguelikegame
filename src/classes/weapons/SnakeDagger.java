package classes.weapons;

import classes.CharacterClass;
import classes.Enemy;
import classes.Weapon;
import classes.gameCore.GameCore;

public class SnakeDagger extends Weapon {
    public SnakeDagger() {
        this.setWeaponName("Змеиный кинжал");
        this.setWeaponAttackValue(6);
        this.setWeaponSkillName("Быстрая серия ударов");
        this.setSpellManacost(6);
    }

    @Override
    public boolean weaponSkill(CharacterClass hero, Enemy enemy) {
        if (hero.getManaPoints() - this.getSpellManacost() >= 0) {
            hero.setManaPoints(hero.getManaPoints() - this.getSpellManacost());
            System.out.println(hero.getName() + " совершает " + this.getWeaponSkillName() + ". Наносит 3 быстрых удара кинжалом, наносящие по 4 урона");
            hero.attack(enemy, 4);
            hero.attack(enemy, 4);
            hero.attack(enemy, 4);
            return true;
        } else {
            GameCore.notЕnoughManaAlert();
            return false;
        }
    }
}
