package classes.weapons;

import classes.CharacterClass;
import classes.Enemy;
import classes.Weapon;
import classes.gameCore.GameCore;

public class BoarClub extends Weapon {
    public BoarClub() {
        this.setWeaponName("Кабанья дубина");
        this.setWeaponAttackValue(9);
        this.setWeaponSkillName("Молитва об исцелении кабаньему богу");
        this.setSpellManacost(10);
    }

    @Override
    public boolean weaponSkill(CharacterClass hero, Enemy enemy) {
        if (hero.getManaPoints() - this.getSpellManacost() >= 0) {
            hero.setManaPoints(hero.getManaPoints() - this.getSpellManacost());
            System.out.println(hero.getName() + " совершает " + this.getWeaponSkillName() + "."+ hero.getName() +" восстанавливает себе 12\uD83D\uDC97");
            hero.setHealthPoints(hero.getHealthPoints() + 12);
            return true;
        } else {
            GameCore.notЕnoughManaAlert();
            return false;
        }
    }
}
