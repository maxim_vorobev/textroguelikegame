package classes.activeSkills;

import classes.CharacterClass;
import classes.Enemy;
import classes.gameCore.GameCore;

public abstract class ActiveSkillsLIB {
    public static boolean devourSoul(CharacterClass hero, Enemy enemy){
        if (hero.getManaPoints() - 60 >= 0) {
            hero.setManaPoints(hero.getManaPoints() - 60);
            System.out.println(hero.getName() + " поглощает душу "+ enemy.getName() +" восстанавливает себе "+enemy.getHealthPoints()+"\uD83D\uDC97");
            hero.setHealthPoints(hero.getHealthPoints() + enemy.getHealthPoints());
            enemy.setHealthPoints(0);
            return true;
        } else {
            GameCore.clearConsole(1);
            System.out.println("⚠\uFE0FНедостаточно маны для навыка оружия!⚠\uFE0F");
            GameCore.clearConsole(1);
            return false;
        }
    }

    public static boolean bloodRituals(CharacterClass hero, Enemy enemy){
        if (hero.getHealthPoints() - 15 > 0) {
            hero.setHealthPoints(hero.getHealthPoints() - 15);
            System.out.println(hero.getName() + " совершает кровавый ритуал\uD83E\uDE78 и наносит себе 15 урона. "+enemy.getName()+" атакуют бесы\uD83D\uDC79\uD83D\uDC79");


            int action = (int) GameCore.getRandomIntegerBetweenRange(4,13);
            System.out.println("\uD83D\uDC79 атакует " + enemy.getName() + " и наносит "+ action +" урона!\uD83D\uDC94");
            enemy.loseHealth(action);

            action = (int) GameCore.getRandomIntegerBetweenRange(4,13);
            System.out.println("\uD83D\uDC79 атакует " + enemy.getName() + " и наносит "+ action +" урона!\uD83D\uDC94");
            enemy.loseHealth(action);

            action = (int) GameCore.getRandomIntegerBetweenRange(4,13);
            System.out.println("\uD83D\uDC79 атакует " + enemy.getName() + " и наносит "+ action +" урона!\uD83D\uDC94");
            enemy.loseHealth(action);

            System.out.println("\uD83D\uDC97У " + enemy.getName() + " осталось "+ enemy.getHealthPoints() + " жизней. \uD83D\uDC97");


            return true;
        } else {
            GameCore.clearConsole(1);
            System.out.println("⚠\uFE0FНедостаточно маны для навыка оружия!⚠\uFE0F");
            GameCore.clearConsole(1);
            return false;
        }
    }
}
