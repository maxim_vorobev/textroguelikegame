package classes;

import classes.characters.Hero;

public interface BaseWeapon {
    boolean weaponSkill(CharacterClass hero, Enemy enemy);
}
