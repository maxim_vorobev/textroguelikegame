package classes;

public abstract class BodyArmor {
    private int armorAmount;
    private String name;
    private int healthRegenAmount;
    private int manaRegenAmount;

    public int getArmorAmount() {
        return armorAmount;
    }

    public void setArmorAmount(int armorAmount) {
        this.armorAmount = armorAmount;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getHealthRegenAmount() {
        return healthRegenAmount;
    }

    public void setHealthRegenAmount(int healthRegenAmount) {
        this.healthRegenAmount = healthRegenAmount;
    }

    public int getManaRegenAmount() {
        return manaRegenAmount;
    }

    public void setManaRegenAmount(int manaRegenAmount) {
        this.manaRegenAmount = manaRegenAmount;
    }
}
