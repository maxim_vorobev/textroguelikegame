package classes;

public interface BaseEnemy {
    void attack(CharacterClass hero);
    void restoreHealth(int amount);
    void loseHealth(int amount);
    void info();
    void skill(CharacterClass hero);
    public void dropLoot(CharacterClass hero);
    public void dropXP(CharacterClass hero);

}
