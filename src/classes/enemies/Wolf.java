package classes.enemies;

import classes.BodyArmor;
import classes.CharacterClass;
import classes.Enemy;
import classes.bodyArmor.WolfSkin;
import classes.gameCore.GameCore;

public class Wolf extends Enemy {
    public Wolf() {
        this.setName("Лесной волк\uD83D\uDC3A");
        this.setAttackAmount(6);
        this.setHealthPoints(15);
        this.setMaxHealthPoints(20);
        this.setExperienceDropAmount(25);
    }

    @Override
    public void skill(CharacterClass hero) {
        System.out.println("Свирепость!\uD83D\uDCA2 Волк\uD83D\uDC3A увеличивает свой урон на 2!");
        this.setAttackAmount(this.getAttackAmount() + 2);
    }

    @Override
    public void attack(CharacterClass hero) {
        int action = (int) GameCore.getRandomIntegerBetweenRange(1,5);

        if (action <2 ){
            this.skill(hero);
        } else {
            System.out.println("\uD83D\uDC94" + this.getName() + " атакует и наносит " + this.getAttackAmount() + " урона! \uD83D\uDC94");
            hero.loseHealth(this.getAttackAmount());
        }

        System.out.println("\uD83D\uDC97У " + hero.getName() + " осталось "+ hero.getHealthPoints() + " жизней. \uD83D\uDC97");
    }

    public void dropLoot(CharacterClass hero){
        int action = (int) GameCore.getRandomIntegerBetweenRange(1,2);

        if (action == 1){
            hero.chooseEquipArmor(new WolfSkin(), this);
        }
    }
}
