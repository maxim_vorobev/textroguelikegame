package classes.enemies;

import classes.CharacterClass;
import classes.Enemy;
import classes.bodyArmor.WolfSkin;
import classes.gameCore.GameCore;
import classes.weapons.SnakeDagger;

public class Snake extends Enemy {
    public Snake() {
        this.setName("Змеюка подколодная\uD83D\uDC0D");
        this.setAttackAmount(8);
        this.setHealthPoints(8);
        this.setMaxHealthPoints(10);
        this.setExperienceDropAmount(20);
    }

    @Override
    public void skill(CharacterClass hero) {
        System.out.println("Змея выпускает ослабляющий яд!☣\uFE0F "+ hero.getName()+" теряет 4 урона на время битвы!");
        hero.setAttackAmount(hero.getAttackAmount() - 4);
    }

    @Override
    public void attack(CharacterClass hero) {
        int action = (int) GameCore.getRandomIntegerBetweenRange(1,5);

        if (action <3 ){
            this.skill(hero);
        } else {
            System.out.println("\uD83D\uDC94" + this.getName() + " атакует и наносит " + this.getAttackAmount() + " урона! \uD83D\uDC94");
            hero.loseHealth(this.getAttackAmount());
        }

        System.out.println("\uD83D\uDC97У " + hero.getName() + " осталось "+ hero.getHealthPoints() + " жизней. \uD83D\uDC97");
    }



    public void dropLoot(CharacterClass hero){
        int action = (int) GameCore.getRandomIntegerBetweenRange(1,2);

        if (action == 1){
            hero.chooseEquipWeapon(new SnakeDagger(), this);
        }
    }
}
