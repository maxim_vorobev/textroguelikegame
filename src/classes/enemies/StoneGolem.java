package classes.enemies;

import classes.CharacterClass;
import classes.Enemy;
import classes.bodyArmor.StoneArmor;
import classes.gameCore.GameCore;
import classes.weapons.SnakeDagger;

public class StoneGolem extends Enemy {
    public StoneGolem() {
        this.setName("Неуязвимый каменный голем\uD83D\uDDFF");
        this.setAttackAmount(5);
        this.setHealthPoints(4);
        this.setMaxHealthPoints(10);
        this.setExperienceDropAmount(30);
    }

    @Override
    public void skill(CharacterClass hero) {
        System.out.println("Голем побрирает с земли груду камней и крепит их на своё тело!\uD83D\uDDFF\uD83D\uDDFF\uD83D\uDDFF "+ this.getName()+" восстанавливает себе 2❤\uFE0F!");
        this.setHealthPoints(this.getHealthPoints() + 2);
    }

    @Override
    public void attack(CharacterClass hero) {
        int action = (int) GameCore.getRandomIntegerBetweenRange(1,7);

        if (action == 1 && (this.getHealthPoints() != this.getMaxHealthPoints())){
            this.skill(hero);
        } else {
            System.out.println("\uD83D\uDC94" + this.getName() + " атакует и наносит " + this.getAttackAmount() + " урона! \uD83D\uDC94");
            hero.loseHealth(this.getAttackAmount());
        }

        System.out.println("\uD83D\uDC97У " + hero.getName() + " осталось "+ hero.getHealthPoints() + " жизней. \uD83D\uDC97");
    }

    public void dropLoot(CharacterClass hero){
        int action = (int) GameCore.getRandomIntegerBetweenRange(1,2);

        if (action == 1){
            hero.chooseEquipArmor(new StoneArmor(), this);
        }
    }

    @Override
    public void loseHealth(int amount) {
        this.setHealthPoints(this.getHealthPoints() - 1);
        if (this.getHealthPoints() < 0) {
            this.setHealthPoints(0);
        }
    }
}
