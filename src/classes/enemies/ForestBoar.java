package classes.enemies;

import classes.CharacterClass;
import classes.Enemy;
import classes.bodyArmor.BoarSkin;
import classes.bodyArmor.WolfSkin;
import classes.gameCore.GameCore;
import classes.weapons.BoarClub;
import classes.weapons.SnakeDagger;

public class ForestBoar extends Enemy {
    public ForestBoar() {
        this.setName("Лесной кабан\uD83D\uDC17");
        this.setAttackAmount(4);
        this.setHealthPoints(25);
        this.setMaxHealthPoints(40);
        this.setExperienceDropAmount(25);
    }

    @Override
    public void skill(CharacterClass hero) {
        System.out.println(this.getName() + " начинает визжать и обваливается в грязи!\uD83D\uDCA9 " + this.getName() + " Восстанавливает себе 10 здоровья❤\uFE0F");
        this.setHealthPoints(this.getHealthPoints() + 10);
    }

    @Override
    public void attack(CharacterClass hero) {
        int action = (int) GameCore.getRandomIntegerBetweenRange(1,7);

        if (action <2 && (this.getHealthPoints() != this.getMaxHealthPoints())){
            this.skill(hero);
        } else {
            System.out.println("\uD83D\uDC94" + this.getName() + " атакует и наносит " + this.getAttackAmount() + " урона! \uD83D\uDC94");
            hero.loseHealth(this.getAttackAmount());
        }

        System.out.println("\uD83D\uDC97У " + hero.getName() + " осталось "+ hero.getHealthPoints() + " жизней. \uD83D\uDC97");
    }

    public void dropLoot(CharacterClass hero){
        int action = (int) GameCore.getRandomIntegerBetweenRange(1,4);

        if (action == 2){
            hero.chooseEquipArmor(new BoarSkin(), this);
        } else if (action == 3){
            hero.chooseEquipWeapon(new BoarClub(), this);
        }
    }
}
