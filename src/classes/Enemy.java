package classes;


public abstract class Enemy implements BaseEnemy{
    private int healthPoints;
    private int attackAmount;
    private String name;
    private int maxHealthPoints;
    private int experienceDropAmount;

    public void dropXP(CharacterClass hero){
        hero.setCurrentXP(hero.getCurrentXP() + this.experienceDropAmount);
    }

    public int getExperienceDropAmount() {
        return experienceDropAmount;
    }

    public void setExperienceDropAmount(int experienceDropAmount) {
        this.experienceDropAmount = experienceDropAmount;
    }

    @Override
    public void attack(CharacterClass hero) {
        System.out.println("\uD83D\uDC94" + this.getName() + " атакует и наносит " + this.getAttackAmount() + " урона\uD83D\uDC94");
        hero.loseHealth(this.attackAmount);
        System.out.println("\uD83D\uDC97У " + hero.getName() + " осталось "+ hero.getHealthPoints() + " жизней\uD83D\uDC97");
    }

    @Override
    public void restoreHealth(int amount) {

    }

    @Override
    public void loseHealth(int amount) {
        this.healthPoints -= amount;
        if (this.healthPoints < 0) {
            this.healthPoints= 0;
        }
    }

    @Override
    public void info() {
        System.out.println("Враг " + this.name + " Здоровье " + this.healthPoints + " Атака " + this.attackAmount);
    }

    public int getHealthPoints() {
        return this.healthPoints;
    }

    public void setHealthPoints(int healthPoints) {
         this.healthPoints = healthPoints;

    }

    public int getAttackAmount() {
        return attackAmount;
    }

    public void setAttackAmount(int attackAmount) {
        this.attackAmount = attackAmount;
        if (this.attackAmount <= 0) this.attackAmount = 1;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getMaxHealthPoints() {
        return maxHealthPoints;
    }

    public void setMaxHealthPoints(int maxHealthPoints) {
        this.maxHealthPoints = maxHealthPoints;
    }
}
