package classes.characters;

import classes.BodyArmor;
import classes.CharacterClass;
import classes.Enemy;
import classes.bodyArmor.BasicArmor;
import classes.weapons.BasicSword;

public class Hero extends CharacterClass {
    public Hero(String name) {
        this.setLevel(1);
        this.setMaxHealthPoints(100);
        this.setMaxManaPoints(100);
        this.setHealthPoints(100);
        this.setManaPoints(12);
        this.setName(name);
        this.setHeroWeapon(new BasicSword());
        this.setArmorAmount(0);
        this.setBodyArmor(new BasicArmor());
        this.setHealthRegenAmount(0);
        this.setManaRegenAmount(1);
        this.setCurrentXP(0);
        this.setBerserkMode(false);
        this.setAnExperiencedSorcerer(false);
        this.setIncreasedReaction(false);
        this.setDevourTheSoul(false);
        this.setBloodRituals(false);
        this.setTotalRegeneration(false);
    }
}
