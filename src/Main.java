import classes.characters.Hero;
import classes.dungeons.Forest;
import classes.gameCore.GameCore;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        System.out.println("Введите название героя:");
        Scanner console = new Scanner(System.in);
        String name = console.nextLine();

        Hero hero = new Hero(name);

        Forest forest = new Forest();
        if(GameCore.startGameLoop(hero, forest)) {
            GameCore.clearConsole(1);
            System.out.println("✅ ✅ ✅ Лес пройден!!! ✅ ✅ ✅");
        } else {
            System.out.println("\uD83C\uDF32☠\uFE0F\uD83C\uDF32Лес победил\uD83C\uDF32☠\uFE0F\uD83C\uDF32");
        }

    }
}